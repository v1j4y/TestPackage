
(* Specify build settings such as compiler and linker flags, libraries to be linked, etc. here *)

Switch[$OperatingSystem,
  "MacOSX", (* Compilation settings for OS X *)
  $buildSettings = {
    "CompileOptions" -> {(* "-std=c++14", "-mmacosx-version-min=10.9" *)},

    (* Statically link the tree library *)
    "ExtraObjectFiles" -> {"/Users/chilkuri/Documents/postdoc/sandbox/python/guga_ice/lib/libgugaice.dylib"},

    (* Tree path *)
    "IncludeDirectories" -> {"/Users/chilkuri/Documents/postdoc/sandbox/python/guga_ice/lib/Tree","/Users/chilkuri/Documents/postdoc/sandbox/python/guga_ice/lib/Tools"},
    "LibraryDirectories" -> {"/Users/chilkuri/Documents/postdoc/sandbox/python/guga_ice/lib/"}
  },

  "Unix", (* Compilation settings for Linux *)
  $buildSettings = {
    "CompileOptions" -> {(* "-std=c++14" *)}

    (*
    , "IncludeDirectories" -> {}
    , "LibraryDirectories" -> {}
    *)
  },

  "Windows", (* Compilation settings for Windows *)
  $buildSettings = { 
    "CompileOptions" -> {"/EHsc", "/wd4244", "/DNOMINMAX"}

    (*
    , "IncludeDirectories" -> {}
    , "LibraryDirectories" -> {}
    *)
  }
]

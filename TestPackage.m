Package["TestPackage`"]

PackageImport["TestPackage`LTemplate`"]

PackageExport["TestPackage"]
TestPackage::usage = "TestPackage is used to test MMAs new package system."
TestPackage[] :=Module[{},Return["Hello world: " <> TestPackage`TestPackageVersion[] ]];



(***** Package variables *****)

PackageScope["$packageDirectory"]
$packageDirectory::usage = "$packageDirectory is the directory where TestPackage is installed.";

$packageVersion    = "0.0.1";
$packageDirectory  = DirectoryName[$InputFileName];

$systemID = $SystemID;

(* On OS X libraries use libc++ ABI since M10.4 and libstdc++ ABI up to M10.3.
   We need separate binaries to support M10.3 and earlier.
   http://community.wolfram.com/groups/-/m/t/816033 *)
If[$OperatingSystem === "MacOSX", $systemID = $systemID <> If[$VersionNumber <= 10.3, "-libstdc++", "-libc++"]];

$libraryDirectory  = FileNameJoin[{$packageDirectory, "LibraryResources", $systemID}];
$sourceDirectory   = FileNameJoin[{$packageDirectory, "LibraryResources", "Source"}];
$buildSettingsFile = FileNameJoin[{$packageDirectory, "BuildSettings.m"}];

(* Add $libraryDirectory to $LibraryPath in case package is not installed in $UserBaseDirectory/Applications. *)
If[Not@MemberQ[$LibraryPath, $libraryDirectory],
  PrependTo[$LibraryPath, $libraryDirectory]
]


(***** The library template *****)

template =LTemplate["TestPackage",
  {
    LClass["TestPackage",
      {
        LFun["version", {}, Real]
      }
    ]
  }
];


(***** Load build settings, if present *****)

(* $buildSettings is normally defined in the build settings files.
   When no build settings file is loaded, it must be set to None. *)
$buildSettings = None;
If[FileExistsQ[$buildSettingsFile], Get[$buildSettingsFile] ]


TestPackage`Developer`Recompile::build = "No build settings found. Please check BuildSettings.m."

TestPackage`Developer`Recompile[] :=
    Module[{},
      (* abort compilation if no build settings are present *)
      If[$buildSettings === None,
        Message[TestPackage`Developer`Recompile::build];
        Return[$Failed]
      ];
      (* create directory for binary if it doesn't exist yet *)
      If[Not@DirectoryQ[$libraryDirectory],
        CreateDirectory[$libraryDirectory]
      ];
      (* compile code *)
      SetDirectory[$sourceDirectory];
      CompileTemplate[template, { },
        "ShellCommandFunction" -> Print, "ShellOutputFunction" -> Print,
        "TargetDirectory" -> $libraryDirectory,
        Sequence @@ $buildSettings
      ];
      Print["Compiling2"];
      ResetDirectory[];
      (* load library *)
      loadTestPackage[] (* defined below *)
    ]


loadTestPackage[] :=
    Module[{deps},
      (* mechanism for loading shared library dependencies, if necessary *)
      deps = FileNameJoin[{$libraryDirectory, "dependencies.m"}];
      Check[
        If[FileExistsQ[deps], Get[deps]],
        Return[$Failed]
      ];
      (* load the library *)
      If[LoadTemplate[template] === $Failed,
        Return[$Failed]
      ];
      (* TODO add any other post-loading initialization if necessary *)
    ]


(* Load library, compile if necessary. *)
If[LoadTemplate[template] === $Failed,
  Print[Style["Loading failed, trying to recompile ...", Red]];
  If[TestPackage`Developer`Recompile[] === $Failed
    ,
    Print[Style["Cannot load or compile library. \[SadSmiley] Aborting.", Red]];
    Abort[] (* verified that it is safe to abort while loading a new-style package *)
    ,
    Print[Style["Successfully compiled and loaded the library. \[HappySmiley]", Red]];
  ]
]


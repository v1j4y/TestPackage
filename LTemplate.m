(* ::Package:: *)

(* This is a traditional package that uses BeginPackage instead of Package.
   It loads LTemplate into the correct context. *)

BeginPackage["TestPackage`"]

Print[$InputFileName]

Get["`LTemplate`LTemplatePrivate`"];
ConfigureLTemplate["MessageSymbol" -> TestPackageSym, "LazyLoading" -> False]

EndPackage[]

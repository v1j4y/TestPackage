Package["TestPackage`"]

PackageImport["TestPackage`LTemplate`"]

PackageExport["TestPackageVersion"]
TestPackageVersion::usage = "TestPackageVersion returns the current version of the package."

(***** Definitions of package functions *****)

obj = ""

PackageExport[TestPackageVersion]
SyntaxInformation[TestPackageVersion] = {"ArgumentsPattern" -> {}};
TestPackageVersion[] := Block[{obj = Make["TestPackage"]}, obj@"version"[]] (* TODO example function, remove if not used *)


